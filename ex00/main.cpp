#include "Frac.class.hpp"

int	main() {
	int	a;
	int	b;

	std::cout << "Numerator input:" << std::endl;
	std::cin >> a;
	std::cout << "Denominator input:" << std::endl;
	std::cin >> b;
	std::cout << "Result:" << std::endl;
	Frac	frac(a, b);
	std::cout << frac.reduce() << std::endl;
	return (0);
}
