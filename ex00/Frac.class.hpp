#pragma once

# include <iostream>

class	Frac {
	public:
		Frac() : a(0), b(1) {}
		Frac(int, int);
		~Frac() {}

		Frac	reduce();

		friend std::ostream&	operator<<(std::ostream&, Frac const&);

	private:
		int	a;
		int	b;

		int	gcd();
};
