#include "Frac.class.hpp"

#include <stdexcept>

Frac::Frac(int a, int b) : a(a), b(b) {
	if (b == 0)
		throw std::logic_error("Division by zero");
}

int		Frac::gcd() {
	int p = b;
	int q = (a < 0) ? (-a) : (a);

	while (p != 0 && q != 0)
		if (p > q)
			p = p % q;
		else
			q = q % p;
	return (p + q);
}

Frac	Frac::reduce() {
	int	tmp;

	if (b < 0) {
		a = -a;
		b = -b;
	}
	if (b == 0)
		throw std::logic_error("Divide by zero");
	if (b == 1)
		return (Frac(a, 1));
	if (a == b)
		return (Frac(1, 1));
	tmp = gcd();
	return (Frac(a / tmp, b / tmp));
}

std::ostream&	operator<<(std::ostream& out, Frac const& frac) {
	out << frac.a;
	if (frac.b != 1)
		out << "/" << frac.b;
	return (out);
}
