#include "Poly.class.hpp"

# include <cmath>

Poly::Poly() {
	std::pair<int, int>	monom(0, 0);

	for (int i = 0; i < 10; ++i)
		members.push_back(monom);
}

int	Poly::calculate(int x) {
	int	result = 0;

	for (int i = 0; i < 10; ++i)
		result += std::pow(x, members[i].second) * members[i].first;
	return (result);
}

std::istream& operator>>(std::istream& in, Poly& poly) {
	std::string	str;
	int			i = 0;
	size_t		offset = 0;
	int			tmp = 0;

	in >> str;
	std::cerr << str << std::endl;
	for (size_t j = 0; str[j];) {
		poly.members[i].first = std::stoi(str.c_str() + j, &offset);
		j += offset;
		if (str[j] == '*') {
			j += 3;
			poly.members[i].second = std::stoi(str.c_str() + j, &offset);
			j += offset;
		}
		++i;
	}
	return (in);
}
