// #106

#pragma once

# include <vector>
# include <iostream>
# include <utility>

class	Poly {
	public:
		Poly();
		~Poly() {}

		int	calculate(int x);

		friend std::istream& operator>>(std::istream&, Poly&);

	private:
		std::vector<std::pair<int, int>>	members;
};
