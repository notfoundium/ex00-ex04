#include "Poly.class.hpp"
#include <fstream>

int main() {
	Poly			poly;
	int				x;
	std::ifstream	fin("IN.txt");
	std::ofstream	fout("OUT.txt");

	fin >> poly;
	fin >> x;
	fout << poly.calculate(x);
	return 0;
}
