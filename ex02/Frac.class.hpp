// #66

#pragma once

# include <iostream>

class	Frac {
	public:
		Frac() : num(0), denom(1) {}
		Frac(int num, int denom) : num(num), denom(denom) {}
		~Frac() {}

		explicit operator float() const {return (float)num / (float)denom;}

		static int	calculate(float, int);

		bool	is_proper() const;

	private:
		int	num;
		int	denom;

		int	gcd() const;
};
