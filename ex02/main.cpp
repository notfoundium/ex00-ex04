#include "Frac.class.hpp"

#include <iostream>

int	main() {
	float	x;
	int		n;

	std::cin >> x;
	std::cin >> n;

	std::cout << Frac::calculate(x, n) << std::endl;

	return (0);
}
