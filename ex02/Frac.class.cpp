#include "Frac.class.hpp"

int	Frac::gcd() const {
	int	a = num;
	int	b = denom;

	while (a != 0 && b != 0)
		if (a > b)
			a %= b;
		else
			b %= a;
	return (a + b);
}

bool	Frac::is_proper() const {
	if (gcd() == 1)
		return (true);
	else
		return (false);
}

int	Frac::calculate(float x, int max_denom) {
	int		count = 0;
	Frac	tmp;

	if (x == 1)
		return (0);
	for (int num_i = 0; num_i < max_denom; ++num_i) {
		for (int denom_i = 0; denom_i <= max_denom; ++denom_i) {
			tmp = Frac(num_i, denom_i);
			if ((float)tmp < x && tmp.is_proper()) {
				++count;
			}
		}
	}
	return (count);
}
