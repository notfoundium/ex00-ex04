#include "Box.class.hpp"

Box::Box(int x1, int y1, int x2, int y2) {
	a1[0] = x1;
	a1[1] = y1;
	a2[0] = x2;
	a2[1] = y2;
}

int	Box::calculate() const {
	int	count_x = 0;
	int	count_y = 0;

	if (a1[0] > a2[0])
		count_x = a1[0] - a2[0] - 1;
	else
		count_x = a2[0] - a1[0] - 1;
	if (a1[1] > a2[1])
		count_y = a1[1] - a2[1] - 1;
	else
		count_y = a2[1] - a1[1] - 1;
	return (count_x * count_y);
}

std::istream&	operator>>(std::istream& in, Box& box) {
	in >> box.a1[0];
	in >> box.a1[1];
	in >> box.a2[0];
	in >> box.a2[1];
	return (in);
}

std::ostream&	operator<<(std::ostream& out, Box const& box) {
	out << box.a1[0] << " ";
	out << box.a1[1] << " ";
	out << box.a2[0] << " ";
	out << box.a2[1] << " ";
	return (out);
}
