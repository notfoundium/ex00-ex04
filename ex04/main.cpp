// #50

#include "Box.class.hpp"
#include <fstream>

int	main() {
	std::ifstream	fin("IN.txt");
	std::ofstream	fout("OUT.txt");
	Box				box;

	fin >> box;
	fout << box.calculate();
	return (0);
}
