#pragma once

# include <iostream>

class	Box {
	public:
		Box() {}
		Box(int x1, int y1, int x2, int y2);
		~Box() {}

		int	calculate() const;

		friend std::istream&	operator>>(std::istream&, Box&);
		friend std::ostream&	operator<<(std::ostream&, Box const&);

	private:
		int	a1[2] = {0, 0};
		int	a2[2] = {0, 0};
};
