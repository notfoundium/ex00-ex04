// #18

# include <fstream>
# include "Universe.class.hpp"

int	main() {
	std::ifstream	fin("LIFE.IN");
	std::ofstream	fout("LIFE.OUT");
	Universe		universe;

	fin >> universe;
	universe.process();
	fout << universe;

	return (0);
}
