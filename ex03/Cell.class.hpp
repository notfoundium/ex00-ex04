#pragma once

#define ALIVE true
#define DEAD false

class Cell {
	public:
		Cell(bool state) : state(state) {}

		bool	is_alive() const {return state;}

		void	revive() {state = ALIVE;}
		void	kill() {state = DEAD;}

	private:
		bool	state;
};
