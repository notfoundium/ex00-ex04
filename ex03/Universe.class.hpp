#pragma once

# include "Cell.class.hpp"
# include <string>
# include <vector>
# include <iostream>

class	Universe {
	public:
		Universe() {}
		~Universe() {}

		void	born_phase();
		void	dead_phase();
		int		getNeighborsCount(std::size_t, std::size_t);
		void	process();

		friend	std::ostream&	operator<<(std::ostream&, Universe const&);
		friend	std::istream&	operator>>(std::istream&, Universe&);

	private:
		std::vector<std::vector<Cell>>	map;
		std::vector<std::vector<Cell>>	buf;
		int	p;
};
