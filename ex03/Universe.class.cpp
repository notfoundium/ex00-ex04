# include "Universe.class.hpp"

int		Universe::getNeighborsCount(std::size_t x, std::size_t y) {
	int	count = 0;
	int	i_start = -1;
	int	i_end = 1;
	int	j_start = -1;
	int	j_end = 1;

	if (y == 0)
		i_start = 0;
	if (x == 0)
		j_start = 0;
	if (y == map.size() - 1)
		i_end = 0;
	if (x == map[0].size() - 1)
		j_end = 0;
	for (int i = i_start; i <= i_end; ++i)
		for (int j = j_start; j <= j_end; ++j)
			if (!(i == 0 && j == 0) && map[y + i][x + j].is_alive())
				count++;
	return (count);
}

void	Universe::born_phase() {
	for (std::size_t i = 0; i < map.size(); ++i)
		for (std::size_t j = 0; j < map[i].size(); ++j)
			if (getNeighborsCount(j, i) == 3 && !map[i][j].is_alive())
				buf[i][j].revive();
}

void	Universe::dead_phase() {
	for (std::size_t i = 0; i < map.size(); ++i)
		for (std::size_t j = 0; j < map[i].size(); ++j)
			if ((getNeighborsCount(j, i) < 2 || getNeighborsCount(j, i) > 3) && map[i][j].is_alive())
				buf[i][j].kill();
}

void	Universe::process() {
	for (int i = 0; i < p; ++i) {
		buf = map;
		#ifdef DEBUG
			std::cout << *this;
			std::cout << std::endl << "===" << std::endl;
		#endif
		born_phase();
		dead_phase();
		map = buf;
	}
}

std::ostream&	operator<<(std::ostream& out, Universe const& universe) {
	for (std::size_t i = 0; i < universe.map.size(); ++i) {
		for (std::size_t j = 0; j < universe.map[i].size(); ++j)
			if (universe.map[i][j].is_alive())
				out << '*';
			else
				out << '.';
		out << std::endl;
	}
	return (out);
}

std::istream&	operator>>(std::istream& in, Universe& universe) {
	std::size_t			m;
	std::size_t			n;
	std::vector<Cell>	row;
	char				c;

	in >> m >> n >> universe.p;

	for (std::size_t i = 0; i < m; ++i) {
		for (std::size_t j = 0; j < n; ++j) {
			in >> c;
			if (c == '.')
				row.push_back(Cell(DEAD));
			else
				row.push_back(Cell(ALIVE));
		}
		universe.map.push_back(row);
		row.clear();
	}
	return (in);
}
